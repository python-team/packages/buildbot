buildbot-worker(7) "" "Services Administration"

# NAME

buildbot-worker - worker instances creation, upgrade and management

# DESCRIPTION

Buildbot worker instances are located under individual folders in
_/var/lib/buildbot/workers_. This manual explains how to manage them.

# CREATION

To create a new Buildbot worker instance named _$NAME_, use the following
commands:

```
buildbot-worker create-worker \\
	/var/lib/buildbot/workers/$NAME <master> <name> <passwd>
chown -R buildbot: /var/lib/buildbot/workers/$NAME
```

After which you may start the worker instance. Depending on the init system you
use, this is done differently.

## systemd

To start the instance:

```
systemctl start buildbot-worker@$NAME.service
```

To enable automatic start on boot:

```
systemctl enable buildbot-worker@$NAME.service
```

If you want to tune the way the instance is started (e.g., to change the user
that runs the process), you may use a systemd drop-in:

```
mkdir -p /etc/systemd/system/buildbot-worker@$NAME.service.d
editor /etc/systemd/system/buildbot-worker@$NAME.service.d/user.conf
```

With the following contents:

```
[Service]
User=my-user
Group=my-group
```

## sysvinit

First, you need to edit _/etc/default/buildbot-worker_ and configure your
instance. The syntax is pretty self explanatory, but here is an example:

```
# 1-enabled, 0-disabled
WORKER_ENABLED[1]=1
# short name printed on start/stop
WORKER_NAME[1]="$NAME"
# user to run worker as
WORKER_USER[1]="buildbot"
# basedir to worker (absolute path)
WORKER_BASEDIR[1]="/var/lib/buildbot/workers/$NAME"
# buildbot-worker options
WORKER_OPTIONS[1]=""
# prefix command, i.e. nice, linux32, dchroot
WORKER_PREFIXCMD[1]=""
```

After which, you may start the instance:

```
service buildbot-worker start $NAME
```

By default, it will automatically start on boot.

# UPGRADE

When installing new versions, each worker instance is automatically restarted.
No additional action is required.

## upgrade from 0.8.x

Since version 0.9.0 of Buildbot "slave"-based terminology is deprecated in
favor of "worker"-based terminology.

If you are upgrading from a 0.8.x version, all old buildbot-slave instances in
_/var/lib/buildbot/slaves_ must be manually migrated. The simplest way of doing
that is to create new workers re-using the same _<master>_, _<name>_ and
_<passwd>_ arguments. See the *CREATION* section of this manual for more
details.

Once migrated, you may safely delete the old "slave" directory:

```
rm -rf /var/lib/buildbot/slaves/$NAME
```

# FILES

_/var/lib/buildbot/workers/$NAME_
	Working directory for the _$NAME_ Buildbot worker instance.

_/etc/systemd/system/buildbot-worker@$NAME.service.d_
	Directory containing customization drop-ins for the _$NAME_ Buildbot
	worker instance service.

_/etc/default/buildbot-worker_
	The worker instances services definition for sysvinit users.

# SEE ALSO

*buildbot-worker*(1), *systemctl*(1), *systemd.unit*(5), *service*(8)


# AUTHOR

Robin Jarry <robin@jarry.cc>
