buildbot(7) "" "Services Administration"

# NAME

buildbot - master instances creation, upgrade and management

# DESCRIPTION

Buildbot master instances are located under individual folders in
_/var/lib/buildbot/masters_. This manual explains how to manage them.

# CREATION

To create a new Buildbot master instance named _$NAME_, use the following
commands:

```
buildbot create-master /var/lib/buildbot/masters/$NAME
chown -R buildbot: /var/lib/buildbot/masters/$NAME
```

After which, you should review and edit the _master.cfg.sample_ configuration
file located in the master base directory and rename it to _master.cfg_.

The configuration file syntax is extensively described in
_/usr/share/doc/buildbot/html_ (installed with the *buildbot-doc* package).

*note*
	In the default configuration Buildbot uses a file-based SQLite
	database, stored in the _state.sqlite_ file of the master's base
	directory. If you want to use another database engine, you may need to
	install additional packages (e.g., *postgresql* and *python3-psycopg2*
	to use PostgreSQL). Refer to the Buildbot documentation for the
	associated _master.cfg_ syntax.

*ATTENTION*
	The new Buildbot web dashboard cannot be integrated into Debian yet
	because of policy violation (See: bug #883529). In the meantime, if you
	want to enable it in your configuration, it can be installed with *pip3
	install buildbot-www*.

You may use the following command to validate your configuration file:

```
buildbot checkconfig /var/lib/buildbot/masters/$NAME/master.cfg
```

Once you are satisfied with your configuration file, you may start the Buildbot
master instance. Depending on the init system you use, this is done differently.

## systemd

To start the instance:

```
systemctl start buildbot@$NAME.service
```

To enable automatic start on boot:

```
systemctl enable buildbot@$NAME.service
```

If you want to tune the way the instance is started (e.g., to change
the user that runs the process), you may use a systemd drop-in:

```
mkdir -p /etc/systemd/system/buildbot@$NAME.service.d
editor /etc/systemd/system/buildbot@$NAME.service.d/user.conf
```

With the following contents:

```
[Service]
User=my-user
Group=my-group
```

## sysvinit

First, you need to edit _/etc/default/buildbot_ and configure your instance. The
syntax is pretty self explanatory, but here is an example:

```
# 1-enabled, 0-disabled
MASTER_ENABLED[1]=1
# short name printed on start/stop
MASTER_NAME[1]="$NAME"
# user to run master as
MASTER_USER[1]="buildbot"
# basedir to master (absolute path)
MASTER_BASEDIR[1]="/var/lib/buildbot/masters/$NAME"
# buildbot options
MASTER_OPTIONS[1]=""
# prefix command, i.e. nice, linux32, dchroot
MASTER_PREFIXCMD[1]=""
```

After which, you may start the instance:

```
service buildbot start $NAME
```

By default, it will automatically start on boot.

# UPGRADE

By default, Buildbot master instances are automatically upgraded when installing
new versions. This consists in the following operations for each instance:

- The instance is stopped.
- _buildbot upgrade-master /var/lib/buildbot/masters/$NAME_ is executed.
- The instance is started again.

You may disable the automatic upgrade globally by running:

```
dpkg-reconfigure buildbot
```

Or for a specific master instance by creating a _.no-auto-upgrade_ file in its
base directory:

```
touch /var/lib/buildbot/masters/$NAME/.no-auto-upgrade
```

In any case, the upgrade operation is mandatory and Buildbot will fail to
restart an instance that has not been upgraded.

After upgrading, you will find that Buildbot has created a new
_master.cfg.sample_ file in the instance base directory. You can review it and
check the differences with your own configuration file. But in most cases, it
is safe to delete this file.

## upgrading from 0.8.x

If upgrading from a 0.8.x version, all master instances in must be manually
migrated. This may require a number of changes to the master configuration.
Please refer to the "nine-upgrade" chapter in the documentation for details.
Once you have finished migrating, you may delete the _.no-auto-upgrade_ files
to allow further automatic upgrades to proceed.

The _buildmaster_ service has been renamed to _buildbot_.

Also, to avoid spurious errors on boot, the automatic start of each existing
instance has been "disabled" in _/etc/default/buildbot_ (which was renamed from
_/etc/default/buildmaster_). If you which to use systemd to manage your
instances, please refer to the *CREATION* section of this manual.

# FILES

_/var/lib/buildbot/masters/$NAME_
	Working directory for the _$NAME_ Buildbot master instance.

_/etc/systemd/system/buildbot@$NAME.service.d_
	Directory containing customization drop-ins for the _$NAME_ Buildbot
	master instance service.

_/etc/default/buildbot_
	The master instances services definition for sysvinit users.

# SEE ALSO

*buildbot*(1), *systemctl*(1), *systemd.unit*(5), *service*(8)

# AUTHOR

Robin Jarry <robin@jarry.cc>
