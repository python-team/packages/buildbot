# Dutch buildbot debconf templates translation
# Copyright (C) 2019
# This file is distributed under the same license as the buildbot package.
# Robin Jarry <robin@jarry.cc>, 2019.
# Frans Spiesschaert <Frans.Spiesschaert@yucom.be>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: buildbot_1.7.0-2\n"
"Report-Msgid-Bugs-To: buildbot@packages.debian.org\n"
"POT-Creation-Date: 2019-01-08 11:33+0100\n"
"PO-Revision-Date: 2019-01-19 11:01+0100\n"
"Last-Translator: Frans Spiesschaert <Frans.Spiesschaert@yucom.be>\n"
"Language-Team: Debian Dutch l10n Team <debian-l10n-dutch@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Gtranslator 2.91.7\n"

#. Type: boolean
#. Description
#: ../buildbot.templates:1001
msgid "Upgrade buildbot master instances when installing new versions?"
msgstr ""
"De hoofdexemplaren van buildbot opwaarderen bij het installeren van een "
"nieuwe versie?"

#. Type: boolean
#. Description
#: ../buildbot.templates:1001
msgid ""
"Stop, run ``buildbot upgrade-master'' and restart all buildbot master "
"instances when installing new versions."
msgstr ""
"Alle hoofdexemplaren van buildbot stoppen, ``buildbot upgrade-master'' "
"uitvoeren en deze hoofdexemplaren opnieuw opstarten, wanneer een nieuwe "
"versie geïnstalleerd wordt."

#. Type: boolean
#. Description
#: ../buildbot.templates:1001
msgid ""
"Please note that the ``buildbot upgrade-master'' operation is potentially "
"destructive and it is irreversible. It is not possible to \"downgrade\" an "
"upgraded master instance. For these reasons, some users may want to do "
"backups before doing it manually."
msgstr ""
"Merk op dat de bewerking ``buildbot upgrade-master'' mogelijk een "
"destructief effect kan hebben en dat dit onomkeerbaar is. Het is onmogelijk "
"om een hoofdexemplaar opnieuw te \"degraderen\". Daarom geven sommige "
"gebruikers er de voorkeur aan om eerst reservekopieën te maken en de "
"opwaardering nadien zelf handmatig uit te voeren."

#. Type: boolean
#. Description
#: ../buildbot.templates:1001
msgid ""
"However, this operation is mandatory and buildbot will fail to restart a "
"master instance if it has not been performed. See buildbot(7) for more "
"details."
msgstr ""
"Deze bewerking is nochtans wel verplicht en zolang deze niet werd "
"uitgevoerd, zal buildbot geen hoofdexemplaar meer kunnen heropstarten. "
"Raadpleeg buildbot(7) voor bijkomende informatie."
